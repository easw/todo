# TODO

`todo` is a simple todo.txt manager built to manage a simple problem of mine: I want a deletive, ephemeral todo manager that refreshes my daily goals each day automatically.  

### installation
Stick main.sh somewhere in your path `$ echo $PATH` under the name todo. I personally link to it from the development directory.

### to use
`$ todo`  
Edits your todo.txt

`$ todo [r / repeat / -r / --repeat]`  
Edits your repeat.txt file

`$ todo [c / config / -c / --config]`  
Edits your config file (by default `~/.config/todo/todo.conf`)

`$ todo [h / help / -h / --help]`  
Returns this help message

`$ todo [code / --code]`  
*Edits the script (tread carefuly)*  
It creates a copy of the file before editing and puts it in the config directory (by default `~/.config/todo/`) with the name todo. This can be restored with the `todo code restore` command  
*Beware that everytime you run this command it overwrites the back up, restore is a one time trick!!!*

`$ todo [code / --code] [restore / --restore]`  
Restores the back up as described above

### configuration
The default config is this:
```bash
REPEAT=~/.config/todo/repeat.txt
TODO=~/.config/todo/todo.txt
DATE=~/.config/todo/date
```
You can change the path to each of those files. A usecase I have changing the files to a directory that's synced between my devices.
A note on the DATE file, it just stores the date of the last time you accessed `todo` to compare to the current date and regenerate the todo file based on that.


### how the magic happens
Each time you access `todo` it checks the current date against the date you last accessed the program (as stored in the DATE file). If it is a different day the todo.txt file is regenerated.  
The new todo.txt takes the contents of your repeat.txt and puts it at the start of your todo.txt file without changing anything in the todo.txt file.  
It's simple, but that's how I like it.

## *todo*
- implement weekly repeating tasks
