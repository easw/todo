CONFIG=~/.config/todo
config=~/.config/todo/todo.conf

if [ ! -d "$CONFIG" ]; then
#	echo "Making $CONFIG folder"
	mkdir -p $CONFIG
fi
if [ ! -f "$config" ]; then
#	echo "Setting up config file $config"
	touch $CONFIG/repeat.txt $CONFIG/todo.txt $CONFIG/date
	echo "REPEAT=$CONFIG/repeat.txt" >> $config
	echo "TODO=$CONFIG/todo.txt" >> $config
	echo "DATE=$CONFIG/date" >> $config
fi
source $config
if [ ! -f "$REPEAT" ]; then
#	echo "Creating repeat file $REPEAT"
	touch $REPEAT
fi
if [[ $(date "+%d-%m-%y") -ne $(cat $DATE 2>/dev/null) ]]; then 
#	echo "Creating new todo file $TODO"
	cat $TODO > $CONFIG/tmp
	cat $REPEAT > $TODO
	cat $CONFIG/tmp >> $TODO
	date "+%d-%m-%y" > $DATE
fi
edit=true
if [[ $1 == "repeat" || $1 == "r" || $1 == "-r" || $1 == "--repeat" ]]; then
	FILE=$REPEAT
elif [[ $1 == "config" || $1 == "c" || $1 == "-c" || $1 == "--config" ]]; then
	FILE=$config
elif [[ $1 == "code" || $1 == "--code" ]]; then
	if [[ $2 == "restore" || $2 == "--restore" ]]; then
		cp -f $CONFIG/$(basename "$0") $(dirname "$0")
	fi
	cp -f $0 $CONFIG
	FILE=$0
elif [[ $1 == "-h" || $1 == "--help" || $1 == "help" || $1 == "h" ]]; then
	edit=false
	cat << EOF
$ todo
Edits your todo.txt

$ todo [r / repeat / -r / --repeat]
Edits your repeat.txt file

$ todo [c / config / -c / --config]
Edits your config file (by default ~/.config/todo/todo.conf)

$ todo [h / help / -h / --help]
Returns this help message

$ todo [code / --code]
*Edits the script (tread carefuly)*
It creates a copy of the file before editing and puts it in the config directory (by default ~/.config/todo/) with the name todo. This can be restored with the todo code restore command
*Beware that everytime you run this command it overwrites the back up, restore is a one time trick!!!*

$ todo [code / --code] [restore / --restore]
Restores the back up as described above

EOF
else
	FILE=$TODO
fi
if [ ! -v "$EDITOR" ]; then
	EDITOR=vi
fi
if $edit; then
	$EDITOR $FILE
fi